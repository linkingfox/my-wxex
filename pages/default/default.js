// pages/default/default.js
const app=getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    bannerList:[],
    cateList:[]
  },
  gotoCate(e){
    console.log(e);
    // wx.setStorageSync('cateCode', e.currentTarget.dataset.code)  
    app.globalData.currentCateCode=e.currentTarget.dataset.code;
    wx.switchTab({
      url: '../category/category',
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let _this=this
    console.log("onLoad");
    wx.request({
      url: app.globalData.remoteUrl+'GetGoodsSalePromotionList',
      data:{
        token:"kjxy_Interface_2024"
      },
      method:"POST",
      success(res){
        console.log(res.data.goodsSalePromotionList)
        _this.setData({
          bannerList:res.data.goodsSalePromotionList
        })
      }
    })
    wx.request({
      url: 'http://kjxycode.zisu.edu.cn/interFaceCommonForGoods/GetGoodscateList',
      data:{
        token:"kjxy_Interface_2024"
      },
      method:"POST",
      success(res){
        console.log(res.data.cateList)
        _this.setData({
          cateList:res.data.cateList
        })
        app.globalData.cateList=res.data.cateList
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // console.log("onReady")
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // console.log("onShow")
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // console.log("onHide")
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})