// pages/category/category.js
const app=getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    goodsList:[],
    cateList:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.setData({cateList:app.globalData.cateList})
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // let cateCode=wx.getStorageSync('cateCode')
    let cateCode=app.globalData.currentCateCode;
    let _this=this
    wx.request({
      url: app.globalData.remoteUrl+'GetGoodsListBycate',
      data:{
        token:"kjxy_Interface_2024",
        cateCode:cateCode
      },
      method:"POST",
      success(res){
        console.log(res)
        _this.setData({
          goodsList:res.data.goodsList
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})